FROM bitnami/minideb:buster
MAINTAINER Oleg Lelenkov "o.lelenkov@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

# Устанавливаем московский часовой пояс
RUN ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime && dpkg-reconfigure tzdata

RUN echo 'APT::Install-Recommends 0;' >> /etc/apt/apt.conf.d/02norecommends \
 && echo 'APT::Install-Suggests 0;' >> /etc/apt/apt.conf.d/02norecommends

