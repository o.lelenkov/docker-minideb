ifndef CI_REGISTRY_IMAGE
	CI_REGISTRY_IMAGE := "registry.gitlab.com/o.lelenkov/docker-minideb"
endif

all: build

build:
	@docker build --tag=${CI_REGISTRY_IMAGE}:$(shell cat VERSION) .

release: build
	@docker tag ${CI_REGISTRY_IMAGE}:$(shell cat VERSION) \
		${CI_REGISTRY_IMAGE}:latest

publish: release
	@docker push ${CI_REGISTRY_IMAGE}:$(shell cat VERSION)
	@docker push ${CI_REGISTRY_IMAGE}:latest

